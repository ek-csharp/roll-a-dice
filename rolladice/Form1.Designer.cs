﻿namespace rolladice
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBalance = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pt1 = new System.Windows.Forms.PictureBox();
            this.pt2 = new System.Windows.Forms.PictureBox();
            this.pt3 = new System.Windows.Forms.PictureBox();
            this.pt4 = new System.Windows.Forms.PictureBox();
            this.pt5 = new System.Windows.Forms.PictureBox();
            this.pt6 = new System.Windows.Forms.PictureBox();
            this.txtp1 = new System.Windows.Forms.TextBox();
            this.txtp2 = new System.Windows.Forms.TextBox();
            this.txtp3 = new System.Windows.Forms.TextBox();
            this.txtp4 = new System.Windows.Forms.TextBox();
            this.txtp5 = new System.Windows.Forms.TextBox();
            this.txtp6 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtcost = new System.Windows.Forms.TextBox();
            this.txtrevenue = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtresult = new System.Windows.Forms.TextBox();
            this.btnexit = new System.Windows.Forms.Button();
            this.btnstart = new System.Windows.Forms.Button();
            this.btnstop = new System.Windows.Forms.Button();
            this.btnagain = new System.Windows.Forms.Button();
            this.ptr1 = new System.Windows.Forms.PictureBox();
            this.ptr2 = new System.Windows.Forms.PictureBox();
            this.ptr3 = new System.Windows.Forms.PictureBox();
            this.timer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pt1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pt2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pt3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pt4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pt5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pt6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptr1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptr2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptr3)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Fira Code", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(34, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "Balance";
            // 
            // txtBalance
            // 
            this.txtBalance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBalance.Location = new System.Drawing.Point(151, 39);
            this.txtBalance.Multiline = true;
            this.txtBalance.Name = "txtBalance";
            this.txtBalance.ReadOnly = true;
            this.txtBalance.Size = new System.Drawing.Size(139, 39);
            this.txtBalance.TabIndex = 1;
            this.txtBalance.TabStop = false;
            this.txtBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Fira Code", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.DarkCyan;
            this.label2.Location = new System.Drawing.Point(399, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(172, 44);
            this.label2.TabIndex = 2;
            this.label2.Text = "Dice Game";
            // 
            // pt1
            // 
            this.pt1.Location = new System.Drawing.Point(39, 103);
            this.pt1.Name = "pt1";
            this.pt1.Size = new System.Drawing.Size(104, 92);
            this.pt1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pt1.TabIndex = 3;
            this.pt1.TabStop = false;
            this.pt1.Click += new System.EventHandler(this.pt1_Click);
            // 
            // pt2
            // 
            this.pt2.Location = new System.Drawing.Point(203, 103);
            this.pt2.Name = "pt2";
            this.pt2.Size = new System.Drawing.Size(104, 92);
            this.pt2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pt2.TabIndex = 4;
            this.pt2.TabStop = false;
            this.pt2.Click += new System.EventHandler(this.pt2_Click);
            // 
            // pt3
            // 
            this.pt3.Location = new System.Drawing.Point(367, 103);
            this.pt3.Name = "pt3";
            this.pt3.Size = new System.Drawing.Size(104, 92);
            this.pt3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pt3.TabIndex = 5;
            this.pt3.TabStop = false;
            this.pt3.Click += new System.EventHandler(this.pt3_Click);
            // 
            // pt4
            // 
            this.pt4.Location = new System.Drawing.Point(39, 262);
            this.pt4.Name = "pt4";
            this.pt4.Size = new System.Drawing.Size(104, 92);
            this.pt4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pt4.TabIndex = 6;
            this.pt4.TabStop = false;
            this.pt4.Click += new System.EventHandler(this.pt4_Click);
            // 
            // pt5
            // 
            this.pt5.Location = new System.Drawing.Point(203, 262);
            this.pt5.Name = "pt5";
            this.pt5.Size = new System.Drawing.Size(104, 92);
            this.pt5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pt5.TabIndex = 7;
            this.pt5.TabStop = false;
            this.pt5.Click += new System.EventHandler(this.pt5_Click);
            // 
            // pt6
            // 
            this.pt6.Location = new System.Drawing.Point(367, 262);
            this.pt6.Name = "pt6";
            this.pt6.Size = new System.Drawing.Size(104, 92);
            this.pt6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pt6.TabIndex = 8;
            this.pt6.TabStop = false;
            this.pt6.Click += new System.EventHandler(this.pt6_Click);
            // 
            // txtp1
            // 
            this.txtp1.Font = new System.Drawing.Font("Fira Code", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtp1.Location = new System.Drawing.Point(39, 209);
            this.txtp1.Multiline = true;
            this.txtp1.Name = "txtp1";
            this.txtp1.ReadOnly = true;
            this.txtp1.Size = new System.Drawing.Size(104, 36);
            this.txtp1.TabIndex = 9;
            this.txtp1.TabStop = false;
            this.txtp1.Text = "0";
            this.txtp1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtp2
            // 
            this.txtp2.Font = new System.Drawing.Font("Fira Code", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtp2.Location = new System.Drawing.Point(203, 209);
            this.txtp2.Multiline = true;
            this.txtp2.Name = "txtp2";
            this.txtp2.ReadOnly = true;
            this.txtp2.Size = new System.Drawing.Size(104, 36);
            this.txtp2.TabIndex = 10;
            this.txtp2.TabStop = false;
            this.txtp2.Text = "0";
            this.txtp2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtp3
            // 
            this.txtp3.Font = new System.Drawing.Font("Fira Code", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtp3.Location = new System.Drawing.Point(367, 209);
            this.txtp3.Multiline = true;
            this.txtp3.Name = "txtp3";
            this.txtp3.ReadOnly = true;
            this.txtp3.Size = new System.Drawing.Size(104, 36);
            this.txtp3.TabIndex = 11;
            this.txtp3.TabStop = false;
            this.txtp3.Text = "0";
            this.txtp3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtp4
            // 
            this.txtp4.Font = new System.Drawing.Font("Fira Code", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtp4.Location = new System.Drawing.Point(39, 370);
            this.txtp4.Multiline = true;
            this.txtp4.Name = "txtp4";
            this.txtp4.ReadOnly = true;
            this.txtp4.Size = new System.Drawing.Size(104, 36);
            this.txtp4.TabIndex = 12;
            this.txtp4.TabStop = false;
            this.txtp4.Text = "0";
            this.txtp4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtp5
            // 
            this.txtp5.Font = new System.Drawing.Font("Fira Code", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtp5.Location = new System.Drawing.Point(203, 370);
            this.txtp5.Multiline = true;
            this.txtp5.Name = "txtp5";
            this.txtp5.ReadOnly = true;
            this.txtp5.Size = new System.Drawing.Size(104, 36);
            this.txtp5.TabIndex = 13;
            this.txtp5.TabStop = false;
            this.txtp5.Text = "0";
            this.txtp5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtp6
            // 
            this.txtp6.Font = new System.Drawing.Font("Fira Code", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtp6.Location = new System.Drawing.Point(367, 370);
            this.txtp6.Multiline = true;
            this.txtp6.Name = "txtp6";
            this.txtp6.ReadOnly = true;
            this.txtp6.Size = new System.Drawing.Size(104, 36);
            this.txtp6.TabIndex = 14;
            this.txtp6.TabStop = false;
            this.txtp6.Text = "0";
            this.txtp6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label3.Location = new System.Drawing.Point(35, 458);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 22);
            this.label3.TabIndex = 15;
            this.label3.Text = "Cost";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label4.Location = new System.Drawing.Point(35, 507);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 22);
            this.label4.TabIndex = 16;
            this.label4.Text = "Revenue";
            // 
            // txtcost
            // 
            this.txtcost.Location = new System.Drawing.Point(142, 454);
            this.txtcost.Multiline = true;
            this.txtcost.Name = "txtcost";
            this.txtcost.ReadOnly = true;
            this.txtcost.Size = new System.Drawing.Size(200, 30);
            this.txtcost.TabIndex = 17;
            this.txtcost.TabStop = false;
            this.txtcost.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtrevenue
            // 
            this.txtrevenue.Location = new System.Drawing.Point(142, 503);
            this.txtrevenue.Multiline = true;
            this.txtrevenue.Name = "txtrevenue";
            this.txtrevenue.ReadOnly = true;
            this.txtrevenue.Size = new System.Drawing.Size(200, 30);
            this.txtrevenue.TabIndex = 18;
            this.txtrevenue.TabStop = false;
            this.txtrevenue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Location = new System.Drawing.Point(35, 560);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 22);
            this.label5.TabIndex = 19;
            this.label5.Text = "Result";
            // 
            // txtresult
            // 
            this.txtresult.Location = new System.Drawing.Point(142, 552);
            this.txtresult.Multiline = true;
            this.txtresult.Name = "txtresult";
            this.txtresult.ReadOnly = true;
            this.txtresult.Size = new System.Drawing.Size(200, 30);
            this.txtresult.TabIndex = 20;
            this.txtresult.TabStop = false;
            this.txtresult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnexit
            // 
            this.btnexit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnexit.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btnexit.Location = new System.Drawing.Point(796, 555);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(110, 33);
            this.btnexit.TabIndex = 21;
            this.btnexit.Text = "Exit";
            this.btnexit.UseVisualStyleBackColor = true;
            this.btnexit.Click += new System.EventHandler(this.btnexit_Click);
            // 
            // btnstart
            // 
            this.btnstart.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnstart.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btnstart.Location = new System.Drawing.Point(610, 344);
            this.btnstart.Name = "btnstart";
            this.btnstart.Size = new System.Drawing.Size(110, 33);
            this.btnstart.TabIndex = 22;
            this.btnstart.Text = "Start";
            this.btnstart.UseVisualStyleBackColor = true;
            this.btnstart.Click += new System.EventHandler(this.btnstart_Click);
            // 
            // btnstop
            // 
            this.btnstop.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnstop.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btnstop.Location = new System.Drawing.Point(764, 344);
            this.btnstop.Name = "btnstop";
            this.btnstop.Size = new System.Drawing.Size(110, 33);
            this.btnstop.TabIndex = 23;
            this.btnstop.Text = "Stop";
            this.btnstop.UseVisualStyleBackColor = true;
            this.btnstop.Click += new System.EventHandler(this.btnstop_Click);
            // 
            // btnagain
            // 
            this.btnagain.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnagain.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btnagain.Location = new System.Drawing.Point(685, 397);
            this.btnagain.Name = "btnagain";
            this.btnagain.Size = new System.Drawing.Size(110, 33);
            this.btnagain.TabIndex = 24;
            this.btnagain.Text = "Reset";
            this.btnagain.UseVisualStyleBackColor = true;
            this.btnagain.Click += new System.EventHandler(this.btnagain_Click);
            // 
            // ptr1
            // 
            this.ptr1.Location = new System.Drawing.Point(610, 103);
            this.ptr1.Name = "ptr1";
            this.ptr1.Size = new System.Drawing.Size(104, 92);
            this.ptr1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptr1.TabIndex = 25;
            this.ptr1.TabStop = false;
            // 
            // ptr2
            // 
            this.ptr2.Location = new System.Drawing.Point(764, 103);
            this.ptr2.Name = "ptr2";
            this.ptr2.Size = new System.Drawing.Size(104, 92);
            this.ptr2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptr2.TabIndex = 26;
            this.ptr2.TabStop = false;
            // 
            // ptr3
            // 
            this.ptr3.Location = new System.Drawing.Point(691, 209);
            this.ptr3.Name = "ptr3";
            this.ptr3.Size = new System.Drawing.Size(104, 92);
            this.ptr3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptr3.TabIndex = 27;
            this.ptr3.TabStop = false;
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(932, 604);
            this.Controls.Add(this.ptr3);
            this.Controls.Add(this.ptr2);
            this.Controls.Add(this.ptr1);
            this.Controls.Add(this.btnagain);
            this.Controls.Add(this.btnstop);
            this.Controls.Add(this.btnstart);
            this.Controls.Add(this.btnexit);
            this.Controls.Add(this.txtresult);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtrevenue);
            this.Controls.Add(this.txtcost);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtp6);
            this.Controls.Add(this.txtp5);
            this.Controls.Add(this.txtp4);
            this.Controls.Add(this.txtp3);
            this.Controls.Add(this.txtp2);
            this.Controls.Add(this.txtp1);
            this.Controls.Add(this.pt6);
            this.Controls.Add(this.pt5);
            this.Controls.Add(this.pt4);
            this.Controls.Add(this.pt3);
            this.Controls.Add(this.pt2);
            this.Controls.Add(this.pt1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtBalance);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Roll A Dice";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pt1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pt2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pt3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pt4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pt5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pt6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptr1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptr2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptr3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBalance;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pt1;
        private System.Windows.Forms.PictureBox pt2;
        private System.Windows.Forms.PictureBox pt3;
        private System.Windows.Forms.PictureBox pt4;
        private System.Windows.Forms.PictureBox pt5;
        private System.Windows.Forms.PictureBox pt6;
        private System.Windows.Forms.TextBox txtp1;
        private System.Windows.Forms.TextBox txtp2;
        private System.Windows.Forms.TextBox txtp3;
        private System.Windows.Forms.TextBox txtp4;
        private System.Windows.Forms.TextBox txtp5;
        private System.Windows.Forms.TextBox txtp6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtcost;
        private System.Windows.Forms.TextBox txtrevenue;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtresult;
        private System.Windows.Forms.Button btnexit;
        private System.Windows.Forms.Button btnstart;
        private System.Windows.Forms.Button btnstop;
        private System.Windows.Forms.Button btnagain;
        private System.Windows.Forms.PictureBox ptr1;
        private System.Windows.Forms.PictureBox ptr2;
        private System.Windows.Forms.PictureBox ptr3;
        private System.Windows.Forms.Timer timer;
    }
}

