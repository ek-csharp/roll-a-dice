﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace rolladice
{  
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
        }

        //closing form
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure you want to close this form?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.No)
            {
                e.Cancel = true; // Cancel the form closing event
            }
        }


        // properties
        private double balance = 50000;
        private double[] nums = { 0, 0, 0, 0, 0, 0 };
        private int[] r = {0,0,0};
        private bool[] played = { false, false, false, false, false, false, };

        // setter 
        public void setbalance(double balance)
        {
            this.balance = balance;
        }

      



        // getter
        public double getBalance()
        {
            return balance;
        }

      


        // timer 
        private void timer_Tick(object sender, EventArgs e)
        {
            Random rnd = new Random();

            setImage(rnd.Next(6) + 1, rnd.Next(6) + 1, rnd.Next(6) + 1);
        }


        // other related methods
        public double caclTotal()
        {
            double total = 0;
            for(int i = 0; i < nums.Length; i++)
            {
                total += nums[i];
            }

            return total;
        }


        // generate a number for random image
        //public int generateANumber()
        //{

        //    Random rnd = new Random();

        //    return rnd.Next(6) + 1;
        //}

        // button stop
        private void btnstop_Click(object sender, EventArgs e)
        {
            btnstart.Enabled = true;
            btnstop.Enabled = false;
            btnagain.Enabled = true;
            double win = 0;
            generateThreenumber();
            timer.Enabled = false;

            

            setImage(r[0], r[1], r[2]);

            for(int i = 0; i < 6; i++)
            {
                if (played[i])
                {
                    bool match = false;
                    for(int j = 0; j < 3; j++)
                    {
                        if((i+1) == r[j])
                        {
                            win += nums[i];
                            match = true;
                        }
                      
                    }

                    if (!match)
                    {
                        win -= nums[i];
                    }
                }
            }

            if(win > 0)
            {
                txtrevenue.Text = win.ToString();
                txtresult.Text = "You win!!!";
                MessageBox.Show("You have win "+ win + " Riels");
            }
            else if(win == 0){
                MessageBox.Show("It's draw!!.");
            }
            else
            {
                txtrevenue.Text = win.ToString();
                txtresult.Text = "You lost!!!";
                MessageBox.Show("You have lost " + (win * (-1)) + " Riels");
            }
            setbalance(getBalance() + win);
            
            txtBalance.Text = getBalance().ToString() + " Riels";
        }

        // set images
        public void setImage(int n1, int n2, int n3)
        {
            ptr1.ImageLocation = @"D:\roll\images\" + n1 +".jpg";
            ptr2.ImageLocation = @"D:\roll\images\" + n2 + ".jpg";
            ptr3.ImageLocation = @"D:\roll\images\" + n3 + ".jpg";
        }


        // generate three number final result
        public void generateThreenumber()
        {
            Random rnd = new Random();

            
            r[0] = rnd.Next(6) + 1;
            r[1] = rnd.Next(6) + 1;
            r[2] = rnd.Next(6) + 1;
            
        }

        // get all value in txtp1 - txtp6

        public void setTotalCost()
        {

            nums[0] = Convert.ToDouble(txtp1.Text);
            nums[1] = Convert.ToDouble(txtp2.Text);
            nums[2] = Convert.ToDouble(txtp3.Text);
            nums[3] = Convert.ToDouble(txtp4.Text);
            nums[4] = Convert.ToDouble(txtp5.Text);
            nums[5] = Convert.ToDouble(txtp6.Text);
        }

        // button start
        private void btnstart_Click(object sender, EventArgs e)
        {
          
            
            setTotalCost();

            double total = caclTotal();

            if(total == 0)
            {
                MessageBox.Show("You haven't put the money yet!!!");
            }
            else if(total <= getBalance())
            {

                btnstart.Enabled = false;
                btnstop.Enabled = true;
                btnagain.Enabled = true;

                timer.Enabled = true;
                txtcost.Text = total.ToString();

                // check if user put money
                for (int i = 0; i < 6; i++)
                {
                    if (nums[i] > 0)
                    {
                        played[i] = true;
                    }
                }
            }
            else
            {
                MessageBox.Show("No Enough Money!!! You got only " + getBalance() + " Riels, but you have spent " + total);

            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            btnagain.Enabled = false;
            btnstop.Enabled = false;
            setImage(1, 1, 1);
            txtBalance.Text = getBalance().ToString() + " Riel";

            pt1.ImageLocation = @"D:\roll\images\1.jpg";
            pt2.ImageLocation = @"D:\roll\images\2.jpg";
            pt3.ImageLocation = @"D:\roll\images\3.jpg";
            pt4.ImageLocation = @"D:\roll\images\4.jpg";
            pt5.ImageLocation = @"D:\roll\images\5.jpg";
            pt6.ImageLocation = @"D:\roll\images\6.jpg";
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnagain_Click(object sender, EventArgs e)
        {
            setbalance(50000);
            txtBalance.Text = getBalance().ToString() + " Riels";
            txtp1.Clear();
            txtp2.Clear();
            txtp3.Clear();
            txtp4.Clear();
            txtp5.Clear();
            txtp6.Clear();
            txtcost.Clear();
            txtresult.Clear();
            txtrevenue.Clear();
        }

        private void pt1_Click(object sender, EventArgs e)
        {
            nums[0] += 100;
            txtp1.Text = nums[0].ToString();
        }

        private void pt2_Click(object sender, EventArgs e)
        {
            nums[1] += 100;
            txtp2.Text = nums[1].ToString();
        }

        private void pt3_Click(object sender, EventArgs e)
        {
            nums[2] += 100;
            txtp3.Text = nums[2].ToString();
        }

        private void pt4_Click(object sender, EventArgs e)
        {
            nums[3] += 100;
            txtp4.Text = nums[3].ToString();
        }

        private void pt5_Click(object sender, EventArgs e)
        {
            nums[4] += 100;
            txtp5.Text = nums[4].ToString();
        }

        private void pt6_Click(object sender, EventArgs e)
        {
            nums[5] += 100;
            txtp6.Text = nums[5].ToString();
        }

       

    }
}
